% -*- program: xelatex -*-
%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode

% NOTE 1: To compile this document make sure the following requirements are fullfilled:
% - Fonts (all free/open-source):
% -- Fira Sans: https://github.com/mozilla/Fira
% -- Iosevka: https://be5invis.github.io/Iosevka/
% - xelatex (utf-8, required for fontspec)

% NOTE 2:
% This document still contains odd bits and ends, latex-warnings will probably raise and unused packages might be included in the preamble but the document should compile under Tex Live (tested on MacTex)

% PREAMBLE
\documentclass[11pt,a4paper]{article}
\usepackage[margin=3cm]{geometry}
\usepackage{fontspec} % requires xelatex
\setmainfont[Ligatures=TeX, BoldFont={Fira Sans Medium}]{Fira Sans} % "Ligatures=TeX" to get proper qoutes from ``'' etc in output
\setmonofont[Scale=0.95]{Iosevka}% Scaled down to work better in-line
\usepackage{xunicode} % makes LaTeX unicode commands work in XeLaTeX
\usepackage{xltxtra}
\usepackage{hyperref}
\usepackage{enumitem}
\usepackage{tikz}
\usetikzlibrary{arrows,decorations.pathmorphing,backgrounds,positioning,fit,shapes.symbols}
\usepackage{lipsum}
\usepackage{tcolorbox}
\usepackage[document]{ragged2e}
\usepackage{nameref}

\setlength{\parindent}{0em}
\setlength{\parskip}{0.5em}

\usepackage[hang, small,labelfont=bf,up,textfont=it,up]{caption} % Custom captions under/above floats in tables or figures

% CONTENT
\begin{document}

\section*{METADATA-CONVERTER GUIDE{ }$\bullet${ }\today{ }$\bullet${ }RWAAI} % (fold)
\label{sec:metadata_converter_guide_bullet_today}

\subsection*{Introduction} % (fold)
\label{sub:introduction}

The metadata-converter (tab2imdi.py) is a script that takes a spreadsheet saved as a tab-separated text file (the metadata) together with a template (an IMDI-file used to determine what goes where in the IMDI XML-structure) and creates one IMDI-file from each row in the spreadsheet.

It should work on any operating system that can install Python 3 (see requirements below) and is run in a terminal window (no graphical interface).

Some previous knowledge of the IMDI-standard and tools such as Arbil is assumed.

\subsection*{Requirements} % (fold)
\label{sec:requirements}
\begin{itemize}
	\item \textbf{Spreadsheet editor}, e.g. LibreOffice\footnote{LibreOffice is free, open-source and works on Windows, Mac and Linux but most spreadsheet editors should be able to export to tab-separated plain-text.}, \url{http://www.libreoffice.org}
	\item \textbf{Java}, \url{http://www.java.com} (Needed for Arbil and LAMUS)
	\item \textbf{Arbil}, \url{https://tla.mpi.nl/tools/tla-tools/arbil/} (requires Java)
	\item \textbf{Python 3}, \url{http://www.python.org}
		\begin{itemize}
			\item \textbf{lxml} (required for validation).
		\end{itemize}
	\item \textbf{tab2imdi.py}, \url{https://bitbucket.org/rwaai/metadata-converter}
		\begin{itemize}
			\item \textbf{IMDI\_3.0.xsd}, \url{http://www.mpi.nl/IMDI/schemas/xsd/IMDI_3.0.xsd} (required for validation)
		\end{itemize}
\end{itemize}

\subsection*{Workflow} % (fold)
\label{sub:workflow}

\begin{enumerate}
	\item Put your metadata in a spreadsheet, row 1 contains headers
	\item Export as tab-separated values, UTF-8 encoded
	\item Create an IMDI-template in Arbil
	\item Export/save the IMDI-template
	\item Collect all necessary files in a folder
	\item Create an ouput folder
	\item Run the converter with the necessary flags
\end{enumerate}

% subsection workflow (end)
% section metadata_converter_guide_bullet_today (end)

% section applications (end)

\subsection*{Preparing your spreadsheet} % (fold)
\label{sec:spreadsheet}

\subsubsection*{Requirements: \emph{Spreadsheet editor}} % (fold)
\label{sub:requirements:_spreadsheet editor}

% subsection requirements:_spreadsheet editor (end)

Use a unicode font with good support for your purposes, e.g. IPA support. Also, make sure that your spreadsheet editor is capable of exporting to a UTF-8 encoded plain text file with tab-separated values.

\begin{tcolorbox}
	\begin{quote}
	\textbf{NOTE:} By design, \textbf{any information included in an IMDI-file can be searched and viewed by anyone browsing the corpus tree online}. This means you might want to include the location but not necessarily your note-to-self comments or sensitive information.

	With this in mind, try not to mix sensitive information and non-sensitive information in the same cell of the spreadsheet. Instead, create a separate column for the sensitive information. You will then have the option to exclude the sensitive column when you create a template in Arbil. In this way you can keep sensitive information in the spreadsheet for reference while excluding it from the IMDI-files you upload to the server.
	\end{quote}
\end{tcolorbox}

\textbf{Each row in the spreadsheet corresponds to one metadata/IMDI-file.} Whether an IMDI-file describes a set of data files or a single resource is up to you (e.g. both sides of a digitized cassette could sometimes be covered well enough by the information in one IMDI-file). There are no requirements as to what kinds of information need to be included in the metadata, but in terms of formatting your spreadsheet there are a few things to keep in mind:

\begin{enumerate}
	\item The first row should only contain \textbf{headers}. These label the type of metadata contained in each column, e.g., date of recording, language, see Fig. 1. These headers will also be passed on to the converter during IMDI-creation. Therefore, \textbf{do NOT use spaces in the headers. If needed, use underscores instead}. To make the headers easy to spot, preferably \textbf{use upper case}, e.g. FILE\_ID in \emph{figure \ref{spreadsheet}}.
	\item You also need to \textbf{create a column with unique IDs} that the converter will use for file names for the IMDI-files, e.g. the FILE\_ID column in \emph{figure \ref{spreadsheet}}. If you already have unique IDs for your data files you could for example put those into this column for easier pairing of metdata and data. Make sure that these are unique as your file system will not accept two or more files with identical names in the same directory.
	\item \textbf{Rows 2 and on contain your metadata.} For some types of information the IMDI-standard requires ISO-formats, e.g. date is YYYY-MM-DD (see \emph{figure \ref{spreadsheet}}). Otherwise, type as you normally would for continuous text, such as for longer content descriptions. Refer to the IMDI-specification\footnote{You can also test this in Arbil. Fields that require ISO-formats or follow other criteria (e.g. boolean true/false) will turn red if a value is incorrectly put in.}.
\end{enumerate}

\begin{figure}[!h]
\caption{spreadsheet}
\label{spreadsheet}
\centering
\begin{tikzpicture}%[text centered]
	% EXPLANATION
	\node at (-2.5,0.25) (1) {\footnotesize {Row 1 contains headers}};
	\node at (-2.5,-0.25) (1) {\footnotesize {Rows 2 and on contain metadata}};
	% SPREADSHEET
	% horizontal
	\draw (0,0.5) -- (7,0.5);
	\draw[dotted] (7,0.5) -- (7.25,0.5);

	\draw (0,0) -- (7,0);
	\draw[dotted] (7,0) -- (7.25,0);

	\draw (0,-0.5) -- (7,-0.5);
	\draw[dotted] (7,-0.5) -- (7.25,-0.5);
	% vertical
	\draw (0,0.5) -- (0,-0.75);
	\draw[dotted] (0,-0.75) -- (0,-1);

	\draw (0.75,0.5) -- (0.75,-0.75);
	\draw[dotted] (0.75,-0.75) -- (0.75,-1);

	\draw (2.75,0.5) -- (2.75,-0.75);
	\draw[dotted] (2.75,-0.75) -- (2.75,-1);

	\draw (4.75,0.5) -- (4.75,-0.75);
	\draw[dotted] (4.75,-0.75) -- (4.75,-1);

	\draw (6.75,0.5) -- (6.75,-0.75);
	\draw[dotted] (6.75,-0.75) -- (6.75,-1);
	% text
	\node at (0.375,0.25) (1) {\textbf{\footnotesize {1}}};
	\node at (0.375,-0.25) (1) {\textbf{\footnotesize {2}}};
	% \node at (0.375,-0.75) (1) {\textbf{\footnotesize {3}}};
	\node at (1.75,0.25) (1) {\textbf{\footnotesize {FILE\_ID}}};
	\node at (1.75,-0.25) (1) {\textbf{\footnotesize {interview\_01}}};
	\node at (3.75,0.25) (1) {\textbf{\footnotesize {DATE}}};
	\node at (3.75,-0.25) (1) {\textbf{\footnotesize {1973-04-15}}};
	\node at (5.75,0.25) (1) {\textbf{\footnotesize {LOCATION}}};
	\node at (5.75,-0.25) (1) {\textbf{\footnotesize {Lampang}}};
\end{tikzpicture}
\end{figure}

Make sure that every column that contains information has a header in row 1. When all the metadata has been entered, export the spreadsheet to a UTF-8 encoded tab-separated plain-text file.

\begin{tcolorbox}
\begin{quote}
	\textbf{NOTE:} When you use ``Save as...'' in order to save a tab-separated file in LibreOffice, make sure that ``Character set:'' is set to ``Unicode (UTF-8)'' and that ``Field delimiter:'' is set to ``\{Tab\}'' in the export window that follows after selecting the .csv-format and pressing OK. Otherwise the converter will fail.
\end{quote}
\end{tcolorbox}
% section spreadsheet (end)

\subsection*{Creating the IMDI-template} % (fold)
\label{sec:imdi_template}
\subsubsection*{Requirements: \emph{Java-framework, Arbil (IMDI-editor)}} % (fold)
\label{sub:requirements_emph_java_framework_arbil_imdi_editor}

The template is an actual IMDI-file that you prepare in Arbil, just as if you were creating all your metadata files this way. The difference is that instead of entering metadata here, you just enter the Headers from your spreadsheet in the appropriate fields in Arbil. These will act as \emph{variables} for the converter.

Each row (starting from rows 2 and on) in your spreadsheet corresponds to an IMDI-file. You can choose to include as little or as much of your spreadsheet as you want. Only the columns you select will be transferred to the IMDI-files.

The headers in your spreadsheet will have to be enclosed in \%\% in order for the converter to include them, i.e. LOCATION in your spreadsheet becomes \%\%LOCATION\%\% in Arbil (see \emph{figure \ref{arbil}}).

When you are finished, export the template as an IMDI-file, e.g. `template.imdi' (right-click session/branch → `Export').

Refer to Arbil’s documentation\footnote{\url{https://tla.mpi.nl/tools/tla-tools/arbil/}} and the IMDI-specification\footnote{\url{https://tla.mpi.nl/imdi-metadata/}} for detailed information.

\begin{figure}[!h]
\caption{Headings become variables}
\label{arbil}
\centering
\begin{tikzpicture}[>=stealth]%[text centered]
	% EXPLANATION
	% \node at (-2.5,0.25) (1) {\footnotesize {Row 1 contains headers}};
	% \node at (-2.5,-0.25) (1) {\footnotesize {Rows 2 and on contain metadata}};
	% SPREADSHEET
	% horizontal
	\draw (0,0.5) -- (5,0.5);
	\draw[dotted] (5,0.5) -- (5.25,0.5);

	\draw (0,0) -- (5,0);
	\draw[dotted] (5,0) -- (5.25,0);

	\draw (0,-0.5) -- (5,-0.5);
	\draw[dotted] (5,-0.5) -- (5.25,-0.5);
	% vertical
	\draw (0,0.5) -- (0,-0.75);
	\draw[dotted] (0,-0.75) -- (0,-1);

	\draw (0.75,0.5) -- (0.75,-0.75);
	\draw[dotted] (0.75,-0.75) -- (0.75,-1);

	\draw (2.75,0.5) -- (2.75,-0.75);
	\draw[dotted] (2.75,-0.75) -- (2.75,-1);

	\draw (4.75,0.5) -- (4.75,-0.75);
	\draw[dotted] (4.75,-0.75) -- (4.75,-1);
	% text
	\node at (0.375,0.25) (1) {\textbf{\footnotesize {1}}};
	\node at (0.375,-0.25) (2) {\textbf{\footnotesize {2}}};
	% \node at (0.375,-0.75) (3) {\textbf{\footnotesize {3}}};
	\node at (1.75,0.25) (4) {\textbf{\footnotesize {DATE}}};
	\node at (1.75,-0.25) (5) {\textbf{\footnotesize {1973-04-15}}};
	\node at (3.75,0.25) (6) {\textbf{\footnotesize {LOCATION}}};
	\node at (3.75,-0.25) (7) {\textbf{\footnotesize {Lampang}}};
	\node at (2.5,-1.75) (1) {\textbf{\footnotesize {Spreadsheet}}};

	% SPREADSHEET -> ARBIL ARROW
	\draw [->, very thick] (5.5,-0.2) -- (6,-0.2);

	% ARBIL
	% horizontal
	\draw (6.25,0.75) -- (11.75,0.75);
	\draw[dotted] (11.75,0.75) -- (12,0.75);

	\draw (6.25,0.25) -- (11.75,0.25);
	\draw[dotted] (11.75,0.25) -- (12,0.25);
	\draw (6.25,-0.25) -- (11.75,-0.25);
	\draw[dotted] (11.75,-0.25) -- (12,-0.25);
	\draw (6.25,-0.75) -- (11.75,-0.75);
	\draw[dotted] (11.75,-0.75) -- (12,-0.75);

	% vertical
	\draw (6.25,0.75) -- (6.25,-1);
	\draw[dotted] (6.25,-1) -- (6.25,-1.25);

	\draw (8.95,0.75) -- (8.95,-1);
	\draw[dotted] (8.95,-1) -- (8.95,-1.25);

	\draw (11.65,0.75) -- (11.65,-1);
	\draw[dotted] (11.65,-1) -- (11.65,-1.25);
	% text col1
	\node at (7.15,0.5) (8) {\textbf{\footnotesize {Field Name}}};
	\node at (6.68,0) (9) {\textbf{\footnotesize {Date}}};
	\node at (7.5,-0.5) (10) {\textbf{\footnotesize {Location.Region}}};
	% text col2
	\node at (9.45,0.5) (8) {\textbf{\footnotesize {Value}}};
	\node at (9.95,0) (9) {\textbf{\footnotesize {\%\%DATE\%\%}}};
	\node at (10.3,-0.5) (10) {\textbf{\footnotesize {\%\%LOCATION\%\%}}};
	\node at (9,-1.75) (1) {\textbf{\footnotesize {Arbil}}};
\end{tikzpicture}
\end{figure}

\begin{tcolorbox}
\begin{quote}
	\textbf{NOTE:} For languages, the IMDI-standard expects the user to specify which ISO standard is being used by prefixing it to the language code, e.g., if using ISO639-3, `English' becomes `ISO639-3:eng'. This can be entered directly into the template. If the column containing the three letter ISO code `eng' in your spreadsheet has the header `LANGUAGE\_ISO', the corresponding field in your template in Arbil could be entered as `ISO639-3:\%\%LANGUAGE\_ISO\%\%'. After running the converter, the generated IMDI-file/s should now all have language ISO entered correctly as `ISO639-3:eng'.
\end{quote}
\end{tcolorbox}

% subsection requirements_emph_java_framework_arbil_imdi_editor (end)
% section imdi_template (end)

\subsection*{Running the converter} % (fold)
\label{sec:imdi_converter}

\subsubsection*{Requirements: \emph{Python 3, tab2imdi.py, metadata file, lxml, IMDI\_3.0.xsd}} % (fold)
\label{sub:converter}

Note that quotation marks around the commands should be excluded when typed.

\subsubsection*{Python 3 and lxml} % (fold)
\label{ssub:python}

Python 3 can be installed in a few different ways. If unsure what these are, download the latest installation package for your platform from \url{http://www.python.org}\footnote{On Windows, make sure to check `Add to PATH' during the installation}. This package includes the utility `pip', which can be used to install the lxml module.

After the installation has finished, do the following:
\begin{enumerate}
	\item If you want validation, type `\texttt{pip3 install lxml}'\footnote{Depending on platform and previous Python-installations the command could also be `\texttt{pip install lxml}'. Linux users might opt for the central package manager instead.}, \url{http://lxml.de}. Wait for the installation procedure to finish.
	\item Type `\texttt{python3}'\footnote{Depending on platform and previous Python-installations the command could also be `\texttt{python}'.}, followed by enter. This should return some version information and a \texttt{>>>} prompt, denoting that you are in the Python interpreter environment.
	\item Still in the Python interpreter, type `\texttt{import lxml}', followed by enter. If lxml has been correctly installed, the \texttt{>>>} prompt will return on the next line with no further messages.
	\item Exit the Python interpreter by pressing ctrl+d, alternatively type `\texttt{quit()}' and press enter.
\end{enumerate}

% subsubsection python (end)

\subsubsection*{Why validate?} % (fold)
\label{ssub:why_validate}

Validating the generated IMDI-files at the conversion stage can save a great deal of time and troubleshooting. LAMUS will do this after upload anyway, but will not accept problematic IMDI-files (e.g. a date written as 95/09/23 in the `Date' field means the IMDI-file will be rejected). It is usually easier to find the exact problem when using the converter for validation, since it will return the problematic line within the IMDI-file in an error message. Having passed validation pre-upload ensures LAMUS will accept the IMDI-files.

% subsubsection why_validate (end)

\subsection*{Workflow} % (fold)
\label{sub:converter}

\begin{enumerate}
	\item Collect all the necessary files and folders in a working folder:
		\begin{itemize}
			\item tab2imdi.py
			\item Spreadsheet saved as plain text file with tab-separated values\footnote{Note that Libreoffice will use .csv regardless of separation method. The converter does not care, as long as the file contains tab-separated, UTF-8 encoded text.}
			\item IMDI-template (e.g. template.imdi)
			\item IMDI\_3.0.xsd (optional, required for validation)
		\end{itemize}
	\item In your working folder, create an output folder for the generated IMDI-files (e.g. `imdi').
	\item Run the converter (see \textit{\nameref{sub:converter_syntax}})
	\item Your IMDI-files are now ready for upload.
\end{enumerate}

\begin{tcolorbox}
\begin{quote}
	\textbf{NOTE:} For those who feel a bit in the dark when using a command line interface, everything can be prepared outside of a terminal window (e.g collect the files and create the required folder/s in the graphical interface before running the converter). If you use the same settings ('flags') for the converter every time it is run, you could also copy the command the first time you run it then save it in a text file for pasting into the command line for later. Just make sure you run the converter from the correct folder.
\end{quote}
\end{tcolorbox}

% subsubsection files (end)

\subsection*{Converter syntax} % (fold)
\label{sub:converter_syntax}

The syntax breaks down as follows:

\begin{figure}[!h]
\caption{Command line, converter syntax}
\label{converter}
\centering
\footnotesize
\begin{tikzpicture}
	\node at (-4,-0.65) (1) {\texttt{tab2imdi.py -o NAME -d DIR --validation --xml-schema-file IMDI\_3.0.xsd TEMPLATE.imdi METADATA.csv}};
	\node at (-7.35,-1) (2) {\texttt{-o} specifies file name column};
	\node at (-6.15,-1.35) (3) {\texttt{-d} specifies outpout directory};
	\node at (-3.85,-1.7) (4) {Validation using the xml-schema `IMDI\_3.0.xsd'};
	\node at (0.4,-2.05) (5) {IMDI template};
	\node at (2.4,-2.4) (6) {Metadata file};
\end{tikzpicture}
\end{figure}

\textbf{Example without validation:}

{\footnotesize\texttt{python3 tab2imdi.py -o IMDI\_ID -d kjg\_narrative -s SKIP\_EXPORT template.imdi kjg\_narrative.csv}}

\textbf{Example with validation:}

{\footnotesize\texttt{python3 tab2imdi.py -o IMDI\_ID -d kjg\_narrative -s SKIP\_EXPORT --validate --xml-schema-file IMDI\_3.0.xsd template.imdi kjg\_narrative.csv}}

\subsubsection*{Breakdown of the examples:} % (fold)
\label{ssub:starting_from_the_beginning_of_the_command_line_}

\begin{description}
	\item \textbf{\texttt{-o IMDI\_ID}} Instructs the converter to use a column titled ``IMDI\_ID'' in the original spreadsheet for the IMDI-file name.
	\item \textbf{\texttt{-d kjg\_narrative}} Instructs the converter to use an output folder called ``kjg\_narrative'' for the IMDI-files. It can be can be called anything but it has to be created before running the converter.
	\item \textbf{\texttt{-s SKIP\_EXPORT}} (optional) This is for skipping rows if needed for any reason. If the converter finds any text in the specified column, the corresponding row will not generate an IMDI-file. In the example, the converter checks whether the cells in a column called ``SKIP\_EXPORT'' contains text or not. As long as the converter finds text -- any text -- in the cells of this column, the corresponding rows will be skipped when running the converter (perhaps a comment on why the row should not be exported?).
	\item \textbf{\texttt{--validate --xml-schema-file IMDI\_3.0.xsd}} (optional) Validates the generated IMDI-files using the XML-schema IMDI\_3.0.xsd. This ensures that they conform to the IMDI standard so that LAMUS will accept them.
	\item \textbf{\texttt{template.imdi}} Instructs the converter to use an imdi-file called `template.imdi' as an xml-template. This is the file you prepared in Arbil earlier.
	\item \textbf{\texttt{kjg\_narrative.csv}} This is your metadata, saved in UTF-8 as a tab-separated plain-text file.
\end{description}
% subsubsection starting_from_the_beginning_of_the_command_line_ (end)

\begin{tcolorbox}
\begin{quote}
	\textbf{NOTE:} If the converter fails and reports an error message, check what it says. There are a few common mistakes that are easily fixed.

	\textbf{In your spreadsheet check for:}
	\begin{itemize}
		\item Mistyped column headers (check for spaces, typos)
		\item Columns that contain information but have no headers in the first row.
		\item Some information, such as date, is not using the expected ISO format.
	\end{itemize}
	\textbf{In your template (using Arbil), check for:}
	\begin{itemize}
		\item Column headers that are not properly enclosed in \%\%, e.g. \%DATE\%, rather than the correct \%\%DATE\%\%.
		\item Mistyped headers that consequently do not correspond to any header in the spreadsheet.
		\item ISO codes that are not entered correctly. ISO for language needs to be specified, e.g. `ISO639-3:' (see the language note, last in the IMDI-template section).
	\end{itemize}
\end{quote}
\end{tcolorbox}

% subsection converter_syntax (end)

% subsection converter (end)

% section imdi_converter (end)

% \section{Troubleshooting} % (fold)
% \label{sec:troubleshooting}

% TO BE ADDED

% section troubleshooting (end)

\end{document}
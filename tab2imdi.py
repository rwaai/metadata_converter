#!/usr/bin/python3

################################################################################
### Copyright (c) 2007, 2012, 2013 Marcus Uneson, marcus.uneson@ling.lu.se
###
### This program is free software; you can redistribute it and/or modify
### it under the terms of the GNU General Public License as published by
### the Free Software Foundation; either version 2 of the License, or
### (at your option) any later version.
###
### This program is distributed in the hope that it will be useful,
### but WITHOUT ANY WARRANTY; without even the implied warranty of
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
### GNU General Public License for more details.
################################################################################

### IMDI metadata conversion utility
### -- takes a variablified imdi template and a n-line tab-separated file with
### 1 line of headings and n-1 records of variable metadata; outputs n-1 imdi files
###
### author: Marcus Uneson, marcus.uneson@ling.lu.se
### 2013-08-19: better error messages for template fieldnames not found in data
### 2013-03-11 workaround for Lamus bug (removed namespace prefixes from output): 
### 2013-03-08: bug fix (xml declaration was missing in output): 
### 2013-03-01: validation (with lxml), skip field: 
### 2012-12-15: csv library with dialects rather than separator flags
### 2012-12-xx: documentation: 
### 2012-11-18: ElementTree rather than string replacement (much safer)
### 2012-10-08: basic port to python from perl: 
### 2007-02-11: perl version
### try -h for usage
### 
###
### TODO AND NOTES:
### currently does not easily handle a non-fixed number of values of a field,
### such as "languages spoken" without some ugly hard-coding -- the
### xml representation of the day should not have to be considered
### (having to resort to the more unwieldy xslt loses the point anyway)
### For very simple cases, just saving "val1;val2;val3" might do
###
### the python implementation uses the csv module, which collects
### all quoting and separator rules in named "dialects", such as 'excel',
### 'unix', 'excel-tab' etc.
### This is likely a better idea than separately specifying quoting etc.
###
### currently, a record is just skipped (with a warning) if the number of
### fields found is unexpected -- could be configurable, for instance also
### to "ignore" and "die" ?
###
### ElementTree leaves namespace prefixes as
### xmlns:ns0="http://www.mpi.nl/IMDI/Schema/IMDI" 
### and <ns0:Location>, <ns0:PlanningType> etc
### Apparently, Lamus does not recognize these -- it expects
### only xmlns="http://www.mpi.nl/IMDI/Schema/IMDI"
### and no prefixes. This is really a bug
###
### Anyway, we'll work around it. 
### The right way to do so, in principle, is to add
### default_namespace="http://www.mpi.nl/IMDI/Schema/IMDI",
### to the call to xmldoc.write(...)
###
### However, this approach is only available in ElementTree 1.3, and
### at any rate it is a bit experimental.
### http://effbot.org/zone/elementtree-13-intro.htm
### 
### Another way is to call, _before any xml input_
### ET.register_namespace('', "http://www.mpi.nl/IMDI/Schema/IMDI")
### http://stackoverflow.com/questions/8983041/saving-xml-files-using-elementtree
###
### It may well be that at most one of these will work on a given system,
### depending on library versions


import argparse
import copy, csv, logging, os, sys
from collections import Counter
import xml.etree.ElementTree as ET
ET.register_namespace('', "http://www.mpi.nl/IMDI/Schema/IMDI")
logging.basicConfig(level=logging.INFO, format="%(levelname)s: %(message)s")

def validate_headings(headings, outfilename):
    if not outfilename in headings:
        raise ValueError("mandatory fieldname '%s' not found " % (outfilename))
    for (i, fieldname) in enumerate(headings, 1):
        if not fieldname:
            raise ValueError("empty fieldname for column %d (1-based index)" % (i))
    c = Counter(headings)
    dups = [k for (k, v) in c.items() if v > 1]
    if len(dups) != 0:
        raise ValueError("duplicated field names %s" % (dups))

def elem_fieldnames(e):
    if e.text and "%%" in e.text:
        fields = e.text.split("%%")[1::2] #all odd-numbered elements;
        return fields                     #if starts with delim, split leaves empty first elem
    else:
        return []

def read_template(fh, imdifn):
    template = ET.parse(fh)
    tplroot = template.getroot()
    c = Counter(fn for e in tplroot.iter() for fn in elem_fieldnames(e))
    logging.info("read template '%s', found the following fieldnames:\n  %s",
                    imdifn, c.most_common())
    return (template, c.keys())

def write_session(recno, template, headings, data, args):
    if not "".join(data).strip(): #empty line
        logging.info("[record %d]: skipping empty record", recno)
        return
    d = dict(zip(headings, data))
    if args.skip_field and d[args.skip_field]:
        logging.info("[record %d]: skipping record w/ field '%s' == true",
            recno, args.skip_field)
        return
    outfile = os.path.join(args.outdir, d[args.outfile_field])
    if not outfile.endswith(".imdi"):
        outfile += ".imdi"
    if os.path.exists(outfile) and not args.overwrite:
        logging.warn("[record %d]: file '%s' already exists (skipping)",
            recno, outfile)
        return

    sessiontpl = copy.deepcopy(template)
    sessionroot = sessiontpl.getroot()
    for e in sessionroot.iter():
        fields = elem_fieldnames(e)
        for k in fields:
            e.text = e.text.replace("%%" + k + "%%", d[k])
    logging.info("[record %d]: writing imdi file '%s'", recno, outfile)
    sessiontpl.write(outfile, 
                     encoding="UTF-8",
                     xml_declaration=True,
                     method="xml"
                     )
    if args.validate:
        args.validator(outfile, recno)
    else:
        logging.info("[record %d]: imdi file '%s' written OK", recno, outfile)

def run(args):
    if not os.path.exists(args.outdir):
        raise IOError("outdir '%s' does not exist -- please create first" % (args.outdir))
    with open(args.imditemplate, encoding="utf-8") as t:
        (template, fields) = read_template(t, args.imditemplate)

    with open(args.tsvfile, encoding="utf-8") as f:
        datas = csv.reader(f, dialect=args.dialect)
        headings = next(datas)
        logging.info("read headings of infile '%s', found %d headings", args.tsvfile, len(headings))
        if len(headings) <= 1:
            logging.warn("I found only %d heading(s) -- wrong separator?", len(headings))
        unknowns = set(fields) - set(headings)
        if unknowns:
            raise ValueError("FATAL: template fields not found in infile: %s" % unknowns)
        logging.debug("headings are: %s", headings)
        validate_headings(headings, args.outfile_field)
        for (i, data) in enumerate(datas):
            write_session(i, template, headings, data, args)

def main():
    args = parse_args()
    run(args)
    return 0

def parse_args():
    parser = argparse.ArgumentParser(description="Convert csv/tsv files "
          "(e.g., exported from spreadsheets) to imdi metadata descriptions.")
    parser.add_argument( '-i', '--intro', action='store_true',
        help="print an introduction")
    parser.add_argument('-v', '--verbosity', metavar="V", type=int, default=1,
        help="verbosity level (0 <= V <= 2; default=1)")
    parser.add_argument('-w', '--overwrite', action='store_true',
        help="silently overwrite existing output file/s")
    parser.add_argument('-D', '--dialect', type=str, metavar="DIAL", default="excel-tab",
        help="tsv dialect (default: excel-tab); must be one of %s (see %s for details)" % (
            csv.list_dialects(), "http://docs.python.org/3.3/library/csv.html"))
    parser.add_argument('-d', '--outdir', metavar="DIR",  default='imdi_output',
        help="use DIR for all imdi output (default='imdi_output')")
    parser.add_argument('-o', '--outfile-field', metavar="FIELD", default="OUTFILE",
        help="FIELD names a (mandatory) field which contains a unique output filename "
        "for each record (default: OUTFILE). "
        "'.imdi' is added to the output filename, if absent.")
    parser.add_argument('-s', '--skip-field', metavar="FIELD", default=None,
        help="FIELD names an optional skip field: if for a record this field has "
             "a true value, then the record will be skipped. "
             "Default is None (feature deactivated)")
    parser.add_argument( '--validate', action='store_true',
        help="if given, each output file is validated (slower, and requires lxml)")
    parser.add_argument('--xml-schema-file', default=None,
        help="(with validation==true). Validate against this xml schema file")
    parser.add_argument('imditemplate',
        help="IMDI template with named variables "
             "(like %%CONSULTANT_AGE%%, %%DATE%%)")
    parser.add_argument('tsvfile',
        help="tab-separated data file with field names in column headings "
             "(like CONSULTANT_AGE, DATE)")
    (args1, rest) = parser.parse_known_args()
    if rest:
        print(rest)
        sys.exit(1)
    if args1.intro:
        print(usage_long())
        sys.exit(0)
    args = parser.parse_args()
    if args.validate:
        args.validator = make_validator(args.xml_schema_file)
    return args

def make_validator(xml_schema_file):
    try:
        from lxml import etree
        xml_schema_doc  = etree.parse(xml_schema_file)
        xml_schema = etree.XMLSchema(xml_schema_doc)

    except ImportError as e:
        logging.fatal("%s \n"
                      "You asked for validation, but this requires lxml (http://lxml.de) "
                      "Either install lxml or run without validation",
                      e)
        sys.exit(1)
    except TypeError as e:
        logging.fatal("You asked for validation, but this requires an xml schema file, e.g. "
                      "http://www.mpi.nl/IMDI/Schema/IMDI_3.0.xsd "
                      "(use --xml-schema-file XSD-FILE)")
        sys.exit(1)
    except IOError as e:
        logging.fatal(e)
        sys.exit(1)

    def validator(sessionfile, recno):
        with open(sessionfile) as f:
            session = etree.parse(f)
        try:
            xml_schema.assertValid(session)
        except etree.DocumentInvalid as e:
            logging.warn("[record %d]: validation error: %s", recno, e)

    return validator



def usage_long():
    return """\
INTRODUCTION

This script might help you in producing IMDI metadata files en masse, from
two kinds of input:

  1) metadata specified in a tab-separated text file, for fields which vary
    from session to session; and
  2) an IMDI template, for fields which do not.

User-defined keys are supported.

If you have complex metadata, it might be insufficient; in particular, it
is much tied to one single template and does not easily handle cases where
elements can vary arbitrarily in number between sessions, such as number
of languages spoken or number of people involved. However, simple cases
are handled OK; and if you can express what you need in one template, it might
be good enough.


TYPICAL USAGE

   1) Create a subdirectory where you wish the output files to go,
   if it does not already exist.

   2a) Pick a corpus you wish to generate metadata for.
   Collect all the metadata which varies between sessions (or
   bundles) in a spreadsheet application, such as Libre/OpenOfficeCalc,
   MS Excel, Gnumeric, GoogleDoc, or similar.
   Put some heading of your choosing over each column,
   using only letters and numbers and underscore ('_'). Typical
   column headings (i.e., field names) might be CONSULTANT_AGE,
   MOTHER_DEAF, RECORDING_DATE, etc. No heading may appear more than once.

   2b) Create one more column in the spreadsheet, called OUTPUT_FILENAME
   (or something else if you do not like this default). With a method of your
   choosing, fill in a unique target filename for each record (probably
   based on some primary key of the spreadsheet, such as an informant or
   session id).

   2c) Now, export what you have to a tab-separated text file. Typically,
   you need to choose Save as... or Export... (or similar). Use
   Unicode as the encoding, if asked. If you can,
   specify tab as the separator.

   3) Use the IMDI editor to create a template containing all
   constant metadata for your set of sessions, and variables of the
   form %%<VARIABLE_NAME>%% for the parts that are not constant.
   For <VARIABLE_NAME> you substitute the appropriate column heading from 2a,
   for instance %%CONSULTANT_AGE%%, %%MOTHER_DEAF%%, etc.
   All variables in the IMDI template must have the corresponding column
   headings (i.e., field names) in the data file. Variables may occur more
   than once in the template (and will then be interpolated more than once).

   4) Call this program with arguments -d <your-output-dir-as-under-1>
   <your-template-file-as-under-3> <your-data-file-as-under-2>.
   If you chose something else than the default under 2b, you will need
   to provide that as well: -o <your-nondefault-filename-field>.
   Similarly, if your fields use another separator than tabs you will
   need to say so: -s <your-separator>. However, tabs are recommended;
   differently from most other standard delimiters, they are unlikely to
   appear in metadata fields, which otherwise may cause confusion.

   5) If all goes well, the program will write one imdi file per record
   in your data file, using the template as (ehm...) template.



NOTES

*  Your imdi file will likely contain links to external resources: media files,
   annotations, images, etc. Precisely where these external resources are
   to be located in the file system is something you should leave to your
   corpus manager.

   When using the IMDI editor in the traditional way, a common solution
   is to put all directories in an external file and refer
   to this file once only in the IMDI file, and then specify individual file
   names only in the IMDI file. However, if your corpus manager has other
   ideas, that's what counts.

   The point here is that none of this is much different when using this
   script; you talk to your corpus manager about paths, then you specify
   the filename in your spreadsheet (instead of in the IMDI file); you
   give that field some name (i.e., column heading); and you put the
   corresponding %%variable%% in the IMDI template.

*  Unused fields in the data file are silently ignored,
   so you may use the same data file with different templates.

*  You can of course produce a tab-separated text file in other ways, too --
   you don't need to use a spreadsheet

*  The variable names follow standard practice ([a-zA-Z0-9_]+). You may wish to
   stick to uppercase as that might make variables easier to spot in the imdi
   xml file, should you need to do that, but it is not required.

*  Blank lines (empty records) in the data file are allowed (and ignored).



"""

if __name__ == '__main__':
    sys.exit(main())
